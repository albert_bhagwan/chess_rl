# What version of CMake is needed?
cmake_minimum_required(VERSION 2.8.3)

# The name of this package.
project(chess_rl)

# Find the catkin build system, and any other packages on which we depend.
find_package(catkin REQUIRED COMPONENTS
    roscpp
    geometry_msgs
    std_msgs
    message_generation
    dynamic_reconfigure
    actionlib
    actionlib_msgs
    trajectory_msgs
    control_msgs
    sensor_msgs
    tf2
    tf2_ros
    chesslab_setup
    gazebo_ros_link_attacher
    robotiq_2f_model
    ur_description
    roslib
    ff
    ur3ik
)

find_package(
   kautham REQUIRED
)

if(KAUTHAM_INCLUDE_DIR)
    message(STATUS "Kautham found and its include dir is $(KAUTHAM_INCLUDE_DIR")
endif()

## Generate MESSAGES in the 'msg' folder
add_message_files(
    FILES
    RobotConfig.msg
    Pose.msg
)

## Generate services in the 'srv' folder
add_service_files(
   FILES
   SetTolerance.srv
   SetTrajectory.srv
   CheckMovAction.srv
   SetRobot.srv
   DmpParser.srv
   PlanChessAction.srv
 )

## Generate added messages and services with any dependencies listed here
 generate_messages(
   DEPENDENCIES
   std_msgs
   actionlib_msgs
   trajectory_msgs
   control_msgs
   sensor_msgs
   geometry_msgs
 )

generate_dynamic_reconfigure_options(cfg/ChessInput.cfg)

# Declare our catkin package.
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES demopkg
   CATKIN_DEPENDS roscpp std_msgs message_runtime chesslab_setup gazebo_ros_link_attacher robotiq_2f_model ur_description  ff ur3ik
#  DEPENDS system_lib
)

# Specify locations of header files.
include_directories(include ${catkin_INCLUDE_DIRS})

add_executable(chess_manager src/chess_manager.cpp)
add_dependencies(chess_manager ${catkin_EXPORTED_TARGETS} ${${PROJECT_NAME}_EXPORTED_TARGETS})
target_link_libraries(chess_manager ${catkin_LIBRARIES})

add_executable(chess_planning src/chess_planning.cpp)
add_dependencies(chess_planning ${catkin_EXPORTED_TARGETS} ${${PROJECT_NAME}_EXPORTED_TARGETS})
target_link_libraries(chess_planning ${catkin_LIBRARIES})

add_executable(demo_rl_1 src/demos/demo_rl_1.cpp)
add_dependencies(demo_rl_1 ${catkin_EXPORTED_TARGETS} ${${PROJECT_NAME}_EXPORTED_TARGETS})
target_link_libraries(demo_rl_1 ${catkin_LIBRARIES})

add_executable(demo_rl_2 src/demos/demo_rl_2.cpp)
add_dependencies(demo_rl_2 ${catkin_EXPORTED_TARGETS} ${${PROJECT_NAME}_EXPORTED_TARGETS})
target_link_libraries(demo_rl_2 ${catkin_LIBRARIES})

add_executable(demo_rl_3 src/demos/demo_rl_3.cpp)
add_dependencies(demo_rl_3 ${catkin_EXPORTED_TARGETS} ${${PROJECT_NAME}_EXPORTED_TARGETS})
target_link_libraries(demo_rl_3 ${catkin_LIBRARIES})

add_executable(save_traj src/save_traj.cpp)
add_dependencies(save_traj ${catkin_EXPORTED_TARGETS} ${${PROJECT_NAME}_EXPORTED_TARGETS})
target_link_libraries(save_traj ${catkin_LIBRARIES})

add_executable(dmp_traj src/dmp_traj.cpp)
add_dependencies(dmp_traj ${catkin_EXPORTED_TARGETS} ${${PROJECT_NAME}_EXPORTED_TARGETS})
target_link_libraries(dmp_traj ${catkin_LIBRARIES})

catkin_install_python(PROGRAMS src/dmp_parser_server.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

catkin_install_python(PROGRAMS src/dmp_offline_server.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
