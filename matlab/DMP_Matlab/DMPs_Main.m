clear; clc; close all;

% General Parameters
dt = 0.01;
tau = 3;
n_rfs = 1000;

dmp_type = 3;
save_on = 0;

% Load and prepare trajectory
Traj1 = table2array(readtable(['Data/traj_carte_',num2str(dmp_type),'m.csv']));
Traj1 = Traj1(:,2:7);
Traj1_d = diff(Traj1)/dt;
Traj1_dd = diff(Traj1_d)/dt;

% Create model and 2nd trajectory
mdl_ur3;
ur3.offset(1) = pi;
UR3_Traj2;



%% Learning weights (each joint)

Y = zeros(size(Traj1,1),3,6);
W = zeros(size(Traj1,1),n_rfs,6);
C = zeros(size(Traj1,1),n_rfs,6);
H = zeros(size(Traj1,1),n_rfs,6);
AMP = zeros(2,1,6);
for coord = 1:6
    ID = coord;
    
    Traj_Oj = [Traj1(:,coord), [0;Traj1_d(:,coord)], [0;0;Traj1_dd(:,coord)]];
    [Y(:,:,coord), W(:,:,coord), C(:,:,coord), H(:,:,coord), AMP(:,:,coord)] = ...
        learn_dcp_fcn(ID, Traj_Oj, tau, n_rfs, dt);
    
end



%% Save to MAT

if (save_on)
    W = permute(W(1,:,:),[2,3,1]);
    C = C(1,:,1)';
    H = H(1,:,1)';
    
    save(['Parameters/Traj1_',num2str(dmp_type),'.mat'],'Traj1');
    save('Parameters/Traj2.mat','Traj2');
    save(['Parameters/W_',num2str(dmp_type),'.mat'],'W');
    save(['Parameters/C_',num2str(dmp_type),'.mat'],'C');
    save(['Parameters/H_',num2str(dmp_type),'.mat'],'H');
    save(['Parameters/AMP_',num2str(dmp_type),'.mat'],'AMP');
end


%% Compare to trajectory
XYZRPY1 = permute(Y(:,1,:), [1,3,2]);

fig7 = figure(7);
fig7.Color = [1,1,1];
fig7.Position = [200   500   1100   350];
coord_names = {'\textbf{X}','\textbf{Y}','\textbf{Z}',...
               '\textbf{Roll}','\textbf{Pitch}','\textbf{Yaw}'};
for coord = 1:6
    subplot(2,3,coord)
    plot(XYZRPY1(:,coord),'LineWidth',1);
    hold on
    plot(Traj1(:,coord),'--','LineWidth',1);
    hold off
    grid on
    set(gca, 'TickLabelInterpreter','latex');
    title(coord_names{coord},'Interpreter','latex','FontSize',12);
    xlim([0 300]);
end

PF1 = XYZRPY1(:,1:3)';

fig8 = figure(8);
fig8.Color = [1,1,1];
fig8.Position = [600   260   650   600];

plot3(PF1(1,:), PF1(2,:), PF1(3,:),'c','LineWidth',1);
grid on;
hold on;
plot3(Traj1(:,1), Traj1(:,2), Traj1(:,3),'--m','LineWidth',2);
hold off;
axis equal;
set(gca, 'TickLabelInterpreter','latex');
fig8.Children.View = [230.0081 25.7806];
legend('Learnt DMP for Movement 2 (Low)','Mean Trajectory for Movement 2 (Low)', ...
       'Interpreter','latex', 'FontSize',12, ...
       'Location','SouthOutside', 'Orientation', 'horizontal');
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);

%fig9 = figure(9);
%fig9.Color = [1,1,1];
%ur3.plot(Q);



%% Run DMP with our code
%runDMP_Clean;



%% Run DMP for different trajectory (2)
xyzrpy_home = Traj2(1,:);
xyzrpy_goal = Traj2(end,:);

Y2 = zeros(tau/dt,3,6);
for ID=1:6
    dcp('reset_state', ID, xyzrpy_home(ID));
    dcp('set_goal', ID, xyzrpy_goal(ID), 1);
    
    for i=0:tau/dt
        
        [y,yd,ydd] = dcp('run',ID,tau,dt);
        
        Y2(i+1,:,ID) = [y yd ydd];
        
    end
end
XYZRPY2 = permute(Y2(:,1,:), [1,3,2]);
PF2 = XYZRPY2(:,1:3)';

fig8 = figure(8);
fig8.Color = [1,1,1];
fig8.Position = [100 200 1200 1000];
plot3(PF1(1,:), PF1(2,:), PF1(3,:),'LineWidth',1);
grid on;
hold on;
plot3(Traj1(:,1), Traj1(:,2), Traj1(:,3),'--','LineWidth',1);
plot3(PF2(1,:), PF2(2,:), PF2(3,:),'LineWidth',1);
%plot3(P_hp2(1,:), P_hp2(2,:), P_hp2(3,:),'--','LineWidth',1);
hold off;
set(gca, 'TickLabelInterpreter','latex');
legend('Obtained trajectory to goal 1','Given trajectory to goal 1', ...
       'Obtained trajectory to goal 2',... %'Theoretical trajectory to goal 2', ...
       'Interpreter','latex', 'FontSize',12, ...
       'Location','SouthOutside', 'Orientation', 'horizontal');
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);

%{
fig9 = figure(9);
fig9.Color = [1,1,1];
for coord = 1:6
    subplot(6,1,coord)
    plot(XYZRPY2(:,coord),'LineWidth',1);
    hold on
    plot(otj2(:,coord),'--','LineWidth',1);
    hold off
    grid on
end
%}

fig10 = figure(10);
fig10.Color = [1,1,1];
for coord = 1:6
    subplot(6,1,coord)
    plot(XYZRPY1(:,coord),'LineWidth',1);
    hold on
    plot(XYZRPY2(:,coord),'LineWidth',1);
    hold off
    grid on
end



