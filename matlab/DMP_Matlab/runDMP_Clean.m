% Needs parameters:
%  AMP, W, C, H 
%  (load data or run DMPs_Test first)

%% General parameters
dt = 0.02;
tau = 2;


%% Load

dmp_type = 3;

load(['Parameters/W_',num2str(dmp_type),'.mat']);
load(['Parameters/C_',num2str(dmp_type),'.mat']);
load(['Parameters/H_',num2str(dmp_type),'.mat']);
load(['Parameters/AMP_',num2str(dmp_type),'.mat']);
load(['Parameters/Traj1_',num2str(dmp_type),'.mat']);

% Compare to traj obtained with python code:
PF_py = table2array(readtable('Results/DMP_result_1.csv'));



%% Execute

% Trajectory to run (can change)
%xyzrpy_home = [0.025 0.395 0.355 -pi 0 pi/2];
%xyzrpy_goal = PF_py(end,:);
xyzrpy_home = Traj1(1,:);
xyzrpy_goal = Traj1(end,:);

% DMP Parameters (always these)
alpha_z = 25;
beta_z  = alpha_z/4;
alpha_g = alpha_z/2;
alpha_x = alpha_z/3;
alpha_v = alpha_z;
beta_v  = beta_z;

c = C;
h = H;

Y2 = zeros(tau/dt,3,6);
for ID=1:6
    
    y0 = xyzrpy_home(ID);
    G = xyzrpy_goal(ID);
    
    A  = AMP(1,:,ID);
    dG = AMP(2,:,ID);
    
    if (A/(abs(dG)+1e-10) > 2.0)
        s = 1;
    else
        s = (G-y0)/dG;
    end
    
    y = y0;
    g = y0;
    z = 0;
    
    x = 1;
    v = 0;
    
    w = W(:,ID);
    
    for i=0:tau/dt

        psi = exp(-0.5*((x-c).^2).*h);
        f = sum(v*w.*psi)/sum(psi+1e-10) * s;
        
        %gt = v*s/sum(psi+1e-10) * psi;
        %f = gt'*w;
        
        vd = (alpha_v*(beta_v*(0-x)-v))/tau;
        xd = v/tau;
        
        zd = (alpha_z*(beta_z*(g-y)-z)+f)/tau;
        yd = z/tau;
        ydd= zd/tau;
        
        gd = alpha_g*(G-g);
        
        v  = v + vd*dt;
        x  = x + xd*dt;
        
        z  = z + zd*dt;
        y  = y + yd*dt;
        
        g  = g + gd*dt;
        
        Y2(i+1,:,ID) = [y yd ydd];
        
        
    end
end
XYZRPY2 = permute(Y2(:,1,:), [1,3,2]);
PF2 = XYZRPY2(:,1:3)';



%% Plot

fig7 = figure(7);
fig7.Color = [1,1,1];
for coord = 1:6
    subplot(6,1,coord)
    plot(XYZRPY2(:,coord),'LineWidth',1);
    hold on
    plot(PF_py(:,coord),'--','LineWidth',1);
    hold off
    grid on
end

fig8 = figure(8);
fig8.Color = [1,1,1];
fig8.Position = [100 200 1200 1000];
plot3(PF2(1,:), PF2(2,:), PF2(3,:),'LineWidth',1);
grid on;
hold on;
plot3(PF_py(:,1), PF_py(:,2), PF_py(:,3), '--', 'LineWidth',1);
hold off;
set(gca, 'TickLabelInterpreter', 'latex');

legend('Obtained trajectory 2 with Matlab', ...
       'Obtained trajectory 2 with Python', ...
       'Interpreter','latex', 'FontSize',12, ...
       'Location','SouthOutside', 'Orientation', 'horizontal');

xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);

%% With robot
%{
mdl_ur3;
ur3.offset(1) = pi;
q_home = [-1.57 -0.78 -1.82 -2.1 1.57 0.0];

hold on
ur3.plot(q_home, 'notiles', 'noshadow', 'nobase', ...
         'jointdiam', 0.7, 'jointlen', 0.8, ...
         'jointcolor', [0.4 0.7 1], 'pjointcolor', [0.7 0 1], ...
         'linkcolor', [0.6 0.6 0.6], 'trail', {'b','LineWidth',1});
hold off

axis equal
zlim([0 0.5]);
%}

