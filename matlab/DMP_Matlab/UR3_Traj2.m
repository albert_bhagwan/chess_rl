
%% General
q_home = [-1.57 -0.78 -1.82 -2.1 1.57 0.0];
T_home = ur3.fkine(q_home);

% Robot axis (from worlx=-y; y=x; z=z);
x_robot = [0 -0.37 -0.04];

%% Another piece
% Robot axis (from world x=-y; y=x; z=z);
x_piece2 = [0.125 -0.075 0.2]; %H2
x_pc2_r = x_piece2 - x_robot;

% From robot base to robot EE
% x = x; y = -y; z = -z
T_pc2_r = [ 1  0  0 x_pc2_r(1);
            0 -1  0 x_pc2_r(2);
            0  0 -1 x_pc2_r(3);
            0  0  0 1];

q_pc2_r = ur3.ikine(T_pc2_r, 'q0', q_home);

jtj2 = jtraj(q_home, q_pc2_r, floor(tau/dt+1));

T_hp2 = ur3.fkine(jtj2).T;
P_hp2 = T_hp2(1:3,4,:);
P_hp2 = permute(P_hp2, [1,3,2]);
RPY_hp2 = tr2rpy(T_hp2);

Traj2 = [P_hp2',RPY_hp2];


