clear; clc; close all;

%{
Res6_0 = table2array(readtable('Results/DMP_result_tau6_0.csv'));
Res6_1 = table2array(readtable('Results/DMP_result_tau6_1.csv'));
Res6_2 = table2array(readtable('Results/DMP_result_tau6_2.csv'));
Res6_3 = table2array(readtable('Results/DMP_result_tau6_3.csv'));

Res2_0 = table2array(readtable('Results/DMP_result_tau2_0.csv'));
Res2_1 = table2array(readtable('Results/DMP_result_tau2_1.csv'));
Res2_2 = table2array(readtable('Results/DMP_result_tau2_2.csv'));
Res2_3 = table2array(readtable('Results/DMP_result_tau2_3.csv'));
%}

Res_0  = table2array(readtable('Results/DMP_result_0.csv'));
Res_1  = table2array(readtable('Results/DMP_result_1.csv'));
Res_2  = table2array(readtable('Results/DMP_result_2.csv'));
Res_3  = table2array(readtable('Results/DMP_result_3.csv'));

Traj_0 = table2array(readtable('Data/traj_carte_0m.csv'));
Traj_1 = table2array(readtable('Data/traj_carte_1m.csv'));
Traj_2 = table2array(readtable('Data/traj_carte_3m.csv'));
Traj_3 = table2array(readtable('Data/traj_carte_4m.csv'));


fig8 = figure(8);
fig8.Color = [1,1,1];
fig8.Position = [100 200 950 700];
plot3(Res_0(:,1), Res_0(:,2), Res_0(:,3),'LineWidth',1);
grid on;
hold on;
plot3(Res_1(:,1), Res_1(:,2), Res_1(:,3),'LineWidth',1);
plot3(Res_2(:,1), Res_2(:,2), Res_2(:,3),'Color',[0,0.8,0],'LineWidth',1);
plot3(Res_3(:,1), Res_3(:,2), Res_3(:,3),'LineWidth',1);

%{
plot3(Traj_0(:,2), Traj_0(:,3), Traj_0(:,4),'--b','LineWidth',2);
plot3(Traj_1(:,2), Traj_1(:,3), Traj_1(:,4),'--r','LineWidth',2);
%}

%plot3(Traj_2(:,2), Traj_2(:,3), Traj_2(:,4),'--m','LineWidth',2);
%plot3(Traj_3(:,2), Traj_3(:,3), Traj_3(:,4),'--m','LineWidth',2);

hold off;

set(gca, 'TickLabelInterpreter', 'latex');

%{
legend('Trajectory 1', 'Trajectory 2', 'Trajectory 3', 'Trajectory 4', ...
       'DMP 1', 'DMP 2', 'Interpreter', 'latex', 'FontSize', 12, ...
       'Location', 'SouthOutside', 'Orientation', 'Horizontal');
%}
legend('Trajectory 1 (Mov.1)', 'Trajectory 2 (Mov.2H)', ...
       'Trajectory 3 (Lin.)', 'Trajectory 4 (Mov.1)', ...
       'Interpreter', 'latex', 'FontSize', 12, ...
       'Location', 'SouthOutside', 'Orientation', 'Horizontal');

axis equal
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);
title('\textbf{DMP Simulation Results}','Interpreter','latex','FontSize',18);

% Gen 1cm vertical lintraj
%{
traj4 = zeros(301,7);

for i=1:301
    traj4(i,1)   = (i-1)/300;
    traj4(i,2:7) = Res2_1(end,:) + (i-1)/300*(Res2_2(end,:)-Res2_1(end,:));
end

writematrix(traj4,'Data/traj_carte_4m.csv');
%}