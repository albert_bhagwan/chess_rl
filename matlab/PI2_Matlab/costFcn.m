function J = costFcn(XYZRPY, ACC, W, obs_pt, hl)

K = size(XYZRPY,3);
Ndt = size(XYZRPY,1);
n_dmp = size(XYZRPY,2);

WQ = 1e8;
WR = 1;


% Viapoint
%{
WV = 1e14;
dt      = 0.01;
via_xyz = [0.048 0.310 0.440];
via_t   =  1.1;
ttv     = round(via_t/dt);
%}

% Obstacle Field
WF = 1e10;
FTh_O = hl - 0.01; % piece height - 2cm pick + 1cm margin

J = zeros(Ndt, K);

for k=1:K
    
    j = zeros(Ndt,1);
    
    for i=1:n_dmp
        
        j = j + 0.5 * WQ * ACC(:,i,k).^2 + ...
                0.5 * WR * sum( (ones(Ndt,1)*W(:,i)').^2 , 2);
        
    end
    
    % Viapoint cost
    %j(ttv) = j(ttv) + WV * sum((XYZRPY(ttv,1:3,k) - via_xyz).^2);
    
    % Field cost
    %{
    XYZk = XYZRPY(:,1:3,k);
    Ft = zeros(Ndt,3);
    for t = 1:Ndt
        PMsh = XYZk(t,:) - Msh;
        dMsh = sqrt(sum(PMsh.^2, 2));
        [dmin, imin] = min(dMsh);
        
        if dmin < FTh_O
            if dmin > FTh_I
                F = FTh_O - dmin;
                Ft(t,:) = [0 0 abs(F)]; %F*PMsh(imin,:)/dmin;
            else
                Ft(t,:) = [0 0 0.48-XYZk(t,3)];
            end
        else
            Ft(t,:) = zeros(1,3);
        end
    end
    
    javoid = WF*sum(abs(Ft),2);
    %}
    
    % Vertical field from point
    %{x
    XYZk = XYZRPY(:,1:3,k);
    
    dPt  = XYZk - obs_pt;
    dist = sum(dPt.^2, 2);
    odw = 1-(dist-min(dist))./range(dist);
    zdw = FTh_O+obs_pt(3) - XYZk(:,3);
    Ft = odw .* zdw.^2;
    
    javoid = WF*abs(Ft);
    %}
    
    % Final cost
    J(:,k) = j + javoid;
    
end

end