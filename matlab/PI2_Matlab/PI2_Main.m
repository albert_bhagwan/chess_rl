clear; clc; close all;


%% Execution settings

% Obtained executions
Res_D1   = table2array(readtable('Results/Demo1/DMP_result_1.csv'));  %E1-E5
Res_D2   = table2array(readtable('Results/Demo2/DMP_result_1.csv'));  %G2-C6

Res_Di = Res_D1;

% Initial and final xy
home_xy = Res_Di(1,1:2);
goal_xy = Res_Di(end,1:2);


% Height of the picked piece (4-8 cm)
hl = 0.08;


% Height of the obstacle (4-8 cm)
obs_hl = 0.08;


% Center of the obstacle (xy)
obs_xy = (Res_Di(1,1:2) + Res_Di(end,1:2))/2;


% DMP parameters
dt = 0.01;
tau = 3;




%% Load Parameters

dmp_type = 3;
load(['Parameters/W_',num2str(dmp_type),'.mat']);
load(['Parameters/C_',num2str(dmp_type),'.mat']);
load(['Parameters/H_',num2str(dmp_type),'.mat']);
load(['Parameters/AMP_',num2str(dmp_type),'.mat']);
load(['Parameters/Traj1_',num2str(dmp_type),'.mat']);

Res_hg  = table2array(readtable('Results/Demo1/DMP_result_1.csv'));


%% Compute other parameters

% Time instants
Ndt = ceil(tau/dt)+1;

% Trajectory
%home = Traj1(1,:);
%goal = Traj1(end,:);
dhl = 0.08 - hl;
home = [home_xy  Res_hg(1,3)-dhl    Traj1(1,4:6)];
goal = [goal_xy  Res_hg(end,3)-dhl  Traj1(end,4:6)];

% Final height considered for the obstacle:
%   (Gripper height+2cm)+virtual extra
obs_dhl = 0.08 - obs_hl;
obs_Tz = Res_hg(1,3)+0.02 - obs_dhl + 0.01;

% Obstacle aruco center
obs_pt = [obs_xy, obs_Tz];
%obs_pt = [0.048 0.360 0.26+0.08+0.04];


%% Original DMP and cost
[XYZRPY0, ACC0] = runDMP_PI2(home,goal,dt,tau, W, C,H,AMP);
J0 = sum(costFcn(XYZRPY0, ACC0, W, obs_pt, hl));


%% Noisy DMP Definition

K = 10;
Sigma = 2;
%gamma = 0.99;
%R = 1e-6;
%L = 1;

n_psi = size(W,1);
n_dmp = size(W,2);

% Noise ponderation
xpe = (1:n_psi)';
npe = 2;
ype = (-(xpe-(n_psi/2)).^npe)./(n_psi/2)^npe + 1;


%% Iterative PI2
NU = 100;

Wnew = W;
Ju = zeros(NU,1);

Weps   = zeros(n_psi, n_dmp, K);

XYZRPY = zeros(Ndt, n_dmp, K);
ACC    = zeros(Ndt, n_dmp, K);
PSI    = zeros(n_psi, Ndt, n_dmp, K);
B      = zeros(n_psi, Ndt, n_dmp, K);

for upd = 1:NU

    for k=1:K
        noise = normrnd(0, Sigma, size(W));
        Weps(:,:,k) = Wnew + ype.*noise;
        
        [XYZRPY(:,:,k), ACC(:,:,k), PSI(:,:,:,k), B(:,:,:,k)] = ...
            runDMP_PI2(home,goal,dt,tau, Weps(:,:,k), C,H,AMP);
    end
    
    Wnew = PI2_Update(XYZRPY, ACC, W, Weps, PSI, B, obs_pt, hl);
    
    % Updated cost
    if mod(upd,10) == 1
        [XYZRPYu, ACCu] = runDMP_PI2(home,goal,dt,tau, Wnew, C,H,AMP);
        Ju(upd) = sum(costFcn(XYZRPYu, ACCu, Wnew, obs_pt, hl));
        fprintf('%4d.Cost = %f\n',upd,Ju(upd));
    end
    
end
[XYZRPYu, ACCu] = runDMP_PI2(home,goal,dt,tau, Wnew, C,H,AMP);
Ju(end) = sum(costFcn(XYZRPYu, ACCu, Wnew, obs_pt, hl));
fprintf('%4d.Cost = %f\n',NU,Ju(end));


%% Plots

fig1 = figure(1);
fig1.Color = [1,1,1];

plot3(XYZRPY(:,1,1), XYZRPY(:,2,1), XYZRPY(:,3,1), 'LineWidth',1);
hold on
for k=1:K
    plot3(XYZRPY(:,1,k), XYZRPY(:,2,k), XYZRPY(:,3,k), 'LineWidth',1);
end
%plot3(Traj1(:,1),Traj1(:,2),Traj1(:,3),'m', 'LineWidth',2)
plot3(XYZRPY0(:,1), XYZRPY0(:,2), XYZRPY0(:,3),'m', 'LineWidth',2);
hold off
grid on
axis equal
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);


fig2 = figure(2);
fig2.Color = [1,1,1];
%plot3(Traj1(:,1),Traj1(:,2),Traj1(:,3),'m', 'LineWidth',2)
plot3(XYZRPY0(:,1), XYZRPY0(:,2), XYZRPY0(:,3),'m', 'LineWidth',2);
hold on
plot3(XYZRPYu(:,1), XYZRPYu(:,2), XYZRPYu(:,3), 'LineWidth',1);
hold off

% Obstacle
obs_color = [1 0 0];
obs_hoffset = -0.01;
plot_piece(obs_pt, obs_hl, obs_hoffset, obs_color)

% Picked piece on PI2 trajectory
%{x
color = [0 0.7 0.9];
hoffset = 0.02;
for i=round(linspace(1,length(XYZRPYu(:,1)),20))
    plot_piece(XYZRPYu(i,1:3), hl, hoffset, color)
end
%}

grid on
axis equal
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12)

% Viapoint
%hold on; scatter3(0.048, 0.31, 0.44); hold off;

% Field
%{
hold on
scatter3(Msh(:,1),Msh(:,2),Msh(:,3),'ob')
quiver3(XYZk(:,1), XYZk(:,2), XYZk(:,3), Ft(:,1), Ft(:,2), Ft(:,3));
hold off
%}

% Python result
%{
XYZRPYpy = table2array(readtable('DMP_PI2_result_3.csv'));
hold on;
plot3(XYZRPYpy(:,1), XYZRPYpy(:,2), XYZRPYpy(:,3), 'LineWidth',1);
hold off
%}






