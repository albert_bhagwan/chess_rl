function Wnew = PI2_Update(XYZRPY, ACC, W, Weps, PSI, B, obs_pt, hl)

K = size(XYZRPY,3);
n_dmp = size(XYZRPY,2);
n_psi = size(W,1);
Ndt = size(XYZRPY,1);

% Call the cost function
J = costFcn(XYZRPY, ACC, W, obs_pt, hl);

% Cumulative cost
S = rot90(rot90(cumsum(rot90(rot90(J)))));

maxS = max(S,[],2);
minS = min(S,[],2);

h = 10; % 1/lambda
expDen = ((maxS-minS)*ones(1,K));
expS = exp(-h*(S - minS*ones(1,K)) ./ expDen);

% Probabilty of a trajectory
P = expS./(sum(expS,2)*ones(1,K));

% Projected noise term 
PMeps = zeros(n_dmp, K, Ndt, n_psi);

for j=1:n_dmp
    
  for k=1:K
    
    % Compute g'*eps in vector form
    basis  = B(:,:,j,k)';
    dth_eps = Weps(:,j,k)' - W(:,j)';
    gTeps = sum(basis .* (ones(Ndt,1)*dth_eps), 2);
    
    % Compute g'g
    gTg = sum(basis.*basis,2);
    
    % Compute P*M*eps = P*g*g'*eps/(g'g) from previous results
    PMeps(j,k,:,:) = basis .* ((P(:,k).*gTeps./(gTg + 1e-10))*ones(1,n_psi));

  end
end

% Compute the parameter update per time step
dthetat = squeeze(sum(PMeps,2));

% Time weighting matrix
NdtW = (Ndt:-1:1)';
TW = (NdtW*ones(1,n_psi)) .* PSI(:,:,1,1)';
TW = TW./(ones(Ndt,1)*sum(TW,1));

% Compute the final parameter update for each DMP
dtheta = squeeze(sum(dthetat.*repmat(reshape(TW,[1,Ndt,n_psi]),[n_dmp 1 1]),2));

% Update the parameters
Wnew = W + dtheta';



end