obs_sl = 0.015;
obs_hl = 0.08;

obs_hoffset = 0.01;

obs_xmin = obs_pt(1) - obs_sl;
obs_xmax = obs_pt(1) + obs_sl;
obs_ymin = obs_pt(2) - obs_sl;
obs_ymax = obs_pt(2) + obs_sl;
obs_zmin = obs_pt(3) - obs_hl - obs_hoffset;
obs_zmax = obs_pt(3) - obs_hoffset;

x_piece = linspace(obs_xmin, obs_xmax, 3);
y_piece = linspace(obs_ymin, obs_ymax, 3);
z_piece = linspace(obs_zmin, obs_zmax, 15);

[X_msh, Y_msh, Z_msh] = meshgrid(x_piece, y_piece, z_piece);
X_msh = X_msh(:);
Y_msh = Y_msh(:);
Z_msh = Z_msh(:);
Msh = [X_msh Y_msh Z_msh];
