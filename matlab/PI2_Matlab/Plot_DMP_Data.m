clear; clc; close all;

%% Load data
nfiles = 10;
dmptype = 3;
for file=1:nfiles
    XYZRPY(:,:,file) = table2array(readtable(['Noisy DMPs/DMP_result_',num2str(file-1),'.csv']));
end

W = load('Parameters/W_3.mat');
W = W.W;
C = load('Parameters/C_3.mat');
C = C.C;
H = load('Parameters/H_3.mat');
H = H.H;
Traj1 = load('Parameters/Traj1_3.mat');
Traj1 = Traj1.Traj1;

fig1 = figure(1);
fig1.Color = [1,1,1];

plot3(XYZRPY(:,1,1), XYZRPY(:,2,1), XYZRPY(:,3,1), 'LineWidth',1);
hold on
for traj=2:nfiles
    plot3(XYZRPY(:,1,traj), XYZRPY(:,2,traj), XYZRPY(:,3,traj), 'LineWidth',1);
end
plot3(Traj1(:,1),Traj1(:,2),Traj1(:,3),'m', 'LineWidth',2)
hold off
grid on
axis equal
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);


