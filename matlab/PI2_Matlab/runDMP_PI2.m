function [XYZRPY,ACC, PSI,B] = runDMP_PI2(home, goal, dt,tau, W,C,H,AMP)

% DMP Parameters (always these)
alpha_z = 25;
beta_z  = alpha_z/4;
alpha_g = alpha_z/2;
%alpha_x = alpha_z/3;
alpha_v = alpha_z;
beta_v  = beta_z;

n_psi = size(W,1);
n_dmp = size(W,2);
Ndt = ceil(tau/dt)+1;

% Run DMP
Y   = zeros(Ndt, 3, n_dmp);
PSI = zeros(n_psi, Ndt, n_dmp);
B   = zeros(n_psi, Ndt, n_dmp);

for ID=1:n_dmp
    
    y0 = home(ID);
    G = goal(ID);
    
    A  = AMP(1,:,ID);
    dG = AMP(2,:,ID);
    
    if (A/(abs(dG)+1e-10) > 2.0)
        s = 1;
    else
        s = (G-y0)/dG;
    end
    
    y = y0;
    g = y0;
    z = 0;
    
    x = 1;
    v = 0;
    
    w = W(:,ID);
    
    for i=0:tau/dt
        
        psi = exp(-0.5*((x-C).^2).*H);
        f = sum(v*w.*psi)/sum(psi+1e-10) * s;
        
        vd = (alpha_v*(beta_v*(0-x)-v))/tau;
        xd = v/tau;
        
        zd = (alpha_z*(beta_z*(g-y)-z)+f)/tau;
        yd = z/tau;
        ydd= zd/tau;
        
        gd = alpha_g*(G-g);
        
        v  = v + vd*dt;
        x  = x + xd*dt;
        
        z  = z + zd*dt;
        y  = y + yd*dt;
        
        g  = g + gd*dt;
        
        Y(i+1,:,ID) = [y yd ydd];
        
        b = v*psi/sum(psi+1.e-10) * s;
        
        PSI(:,i+1,ID) = psi;
        B(:,i+1,ID) = b;
        
        
    end
end

% Get trajectory
XYZRPY = permute(Y(:,1,:), [1,3,2]);
ACC = permute(Y(:,3,:), [1,3,2]);

end


