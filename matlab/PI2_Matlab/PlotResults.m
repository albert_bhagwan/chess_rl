clc; close all;

DemoN = '1';
obs_hl = 0.08;

if DemoN == '3'
    hl = 0.04;
else
    hl = 0.08;
end


% Load results
Res_0  = table2array(readtable(['Results/Demo',DemoN,'/DMP_result_0.csv']));
Res_1  = table2array(readtable(['Results/Demo',DemoN,'/DMP_result_1.csv']));
Res_2  = table2array(readtable(['Results/Demo',DemoN,'/DMP_result_2.csv']));
Res_3  = table2array(readtable(['Results/Demo',DemoN,'/DMP_result_3.csv']));


% Base DMP - Matlab results
dt = 0.01;
tau = 3;
home = Res_1(1,:);
goal = Res_1(end,:);

dmp_type = 3;
load(['Parameters/W_',num2str(dmp_type),'.mat']);
load(['Parameters/C_',num2str(dmp_type),'.mat']);
load(['Parameters/H_',num2str(dmp_type),'.mat']);
load(['Parameters/AMP_',num2str(dmp_type),'.mat']);
load(['Parameters/Traj1_',num2str(dmp_type),'.mat']);

XYZRPY0 = runDMP_PI2(home,goal,dt,tau, W, C,H,AMP);


% Obstacle
Res_hg  = table2array(readtable('Results/Demo1/DMP_result_1.csv'));
obs_xy = (Res_1(1,1:2) + Res_1(end,1:2))/2;
obs_xy(1) = goal(1);
obs_dhl = 0.08 - obs_hl;
obs_Tz = Res_hg(1,3)+0.02 - obs_dhl + 0.01;
obs_pt = [obs_xy, obs_Tz];


%% Figure of whole resulting trajectory
fig1 = figure(1);
fig1.Color = [1,1,1];
fig1.Position = [100 200 1200 600];

plot3(Res_0(:,1), Res_0(:,2), Res_0(:,3),'LineWidth',1);
grid on;
hold on;
% Base DMP
plot3(XYZRPY0(:,1), XYZRPY0(:,2), XYZRPY0(:,3),'--m', 'LineWidth',1);

plot3(Res_1(:,1), Res_1(:,2), Res_1(:,3),'LineWidth',1);
plot3(Res_2(:,1), Res_2(:,2), Res_2(:,3),'Color',[0,0.8,0],'LineWidth',1);
plot3(Res_3(:,1), Res_3(:,2), Res_3(:,3),'LineWidth',1);

% Lower DMP (base of the obstacle)
%plot3(XYZRPY0(:,1), XYZRPY0(:,2), XYZRPY0(:,3)-0.06,'--m', 'LineWidth',2);
%plot3(Res_1(:,1), Res_1(:,2), Res_1(:,3)-0.06,'LineWidth',1);

hold off;

set(gca, 'TickLabelInterpreter', 'latex');

legend('Trajectory 1', 'Trajectory 2 (before PI$^2$)', ...
       'Trajectory 2 (after PI$^2$)', 'Trajectory 3', 'Trajectory 4', ...
       'Interpreter', 'latex', 'FontSize', 12, ...
       'Location', 'SouthOutside', 'Orientation', 'Horizontal');

axis equal
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);

fig1.Children(2).View = [-110 15];


%% Figure of base DMP vs PI2
fig2 = figure(2);
fig2.Color = [1,1,1];
fig2.Position = [100 600 1600 600];

subplot(1,2,1)

% Base DMP
plot3(XYZRPY0(:,1), XYZRPY0(:,2), XYZRPY0(:,3),'--m', 'LineWidth',1);
hold on
plot3(Res_1(:,1), Res_1(:,2), Res_1(:,3),'LineWidth',1);
hold off;

% Obstacle
obs_color = [1 0 0];
obs_hoffset = -0.01;
plot_piece(obs_pt, obs_hl, obs_hoffset, obs_color)

% Picked piece on base DMP
color = [0.9 0 0.7];
hoffset = 0.02;
for i=round(linspace(1,length(Res_1(:,1)),20))
    plot_piece(XYZRPY0(i,1:3), hl, hoffset, color)
end

set(gca, 'TickLabelInterpreter', 'latex');

legend('Before PI$^2$', 'After PI$^2$', ...
       'Interpreter', 'latex', 'FontSize', 12, ...
       'Location', 'SouthOutside', 'Orientation', 'Horizontal');
grid on;
axis equal
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);


subplot(1,2,2)

% Base DMP
plot3(XYZRPY0(:,1), XYZRPY0(:,2), XYZRPY0(:,3),'--m', 'LineWidth',1);
hold on
plot3(Res_1(:,1), Res_1(:,2), Res_1(:,3),'LineWidth',1);
%plot3(XYZRPYu(:,1), XYZRPYu(:,2), XYZRPYu(:,3), 'LineWidth',1);
hold off;

% Obstacle
obs_color = [1 0 0];
obs_hoffset = -0.01;
plot_piece(obs_pt, obs_hl, obs_hoffset, obs_color)

% Picked piece on PI2 trajectory
color = [0 0.7 0.9];
hoffset = 0.02;
for i=round(linspace(1,length(Res_1(:,1)),20))
    plot_piece(Res_1(i,1:3), hl, hoffset, color)
    %plot_piece(XYZRPYu(i,1:3), hl, hoffset, color)
end

set(gca, 'TickLabelInterpreter', 'latex');

legend('Before PI$^2$', 'After PI$^2$', ...
       'Interpreter', 'latex', 'FontSize', 12, ...
       'Location', 'SouthOutside', 'Orientation', 'Horizontal');
grid on;
axis equal
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);

if DemoN == '2'
    fig2.Children(2).View = [-30 10];
    fig2.Children(4).View = [-30 10];
else
    fig2.Children(2).View = [-80 10];
    fig2.Children(4).View = [-80 10];
end

