%% Roll Plot

fig1 = figure(1);
fig1.Color = [1,1,1];
fig1.Position = [458 585 437 362];

%subplot(1,2,2)
hold on
for traj=1:nfiles
    plot(NXYZRPY(:,1,traj), NXYZRPY(:,5,traj))
end
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');

hold on
plot([0;1],[pi;pi],':k','LineWidth',2)
plot([0;1],[-pi;-pi],':k','LineWidth',2)
hold off
ylabel('Roll value','Interpreter','latex','FontSize',12)
%title('\textbf{Raw evolution of Roll}','Interpreter','latex','FontSize',14)


%% Cleaning Plot

fig1 = figure(1);
fig1.Color = [1,1,1];
fig1.Position = [140   420   800   450];

hold on
for traj=1:nfiles
    l1 = plot(RawData{traj}(:,1), RawData{traj}(:,2), 'r', 'LineWidth',0.5);
    l2 = plot(ttXYZRPY{traj}(:,1), ttXYZRPY{traj}(:,2), 'b', 'LineWidth',1);
end
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('Time','Interpreter','latex','FontSize',12)
ylabel('X Coordinate','Interpreter','latex','FontSize',12)
legend([l1 l2], 'Raw Data', 'Clean Data', 'Interpreter','latex',...
    'FontSize',12,'Location','SouthOutside','Orientation','horizontal')
title('\textbf{Cleaning data over time: X coordinate}','Interpreter','latex','FontSize',14)

fig2 = figure(2);
fig2.Color = [1,1,1];
fig2.Position = [940   420   630   450];

hold on
for traj=1:nfiles
    l2 = plot3(ttXYZRPY{traj}(:,2), ttXYZRPY{traj}(:,3), ttXYZRPY{traj}(:,4), ...
        '-b','LineWidth',1);
    l1 = plot3(RawData{traj}(:,2), RawData{traj}(:,3), RawData{traj}(:,4), ...
        '--r', 'LineWidth',1);
end
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);
title('\textbf{3D Trajectory: Raw vs Clean}','Interpreter','latex','FontSize',14)

fig2.Children.View = [250 22];
axis equal
legend([l1 l2], 'Raw Data', 'Clean Data', 'Interpreter','latex',...
    'FontSize',12,'Location','SouthOutside','Orientation','horizontal')

 
%% Normalize time plot

fig1 = figure(1);
fig1.Color = [1,1,1];
fig1.Position = [550   450   550   450];

subplot(2,1,1)
hold on
for traj=1:nfiles
    l1 = plot(ttXYZRPY{traj}(:,1), ttXYZRPY{traj}(:,3), 'LineWidth',1);
end
hold off

grid on
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('Time','Interpreter','latex','FontSize',12)
ylabel('Y Coordinate','Interpreter','latex','FontSize',12)


subplot(2,1,2)
hold on
for traj=1:nfiles
    l2 = plot(tXYZRPY{traj}(:,1), tXYZRPY{traj}(:,3), 'LineWidth',1);
end
hold off

grid on
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('Time (normalized)','Interpreter','latex','FontSize',12)
ylabel('Y Coordinate','Interpreter','latex','FontSize',12)


%% Oversample plot

fig1 = figure(1);
fig1.Color = [1,1,1];
fig1.Position = [250   450   800   450];

subplot(2,1,1)
hold on
for traj=1:nfiles
    l1 = plot(tXYZRPY{traj}(:,1), tXYZRPY{traj}(:,3), 'o-', 'LineWidth', 1);
end
hold off

grid on
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('Time (normalized)','Interpreter','latex','FontSize',12)
ylabel('Y Coordinate','Interpreter','latex','FontSize',12)
title('\textbf{Normalizing and resampling: Y coordinate}','Interpreter','latex','FontSize',16)


subplot(2,1,2)
hold on
for traj=1:nfiles
    l2 = plot(NXYZRPY(:,1,traj), NXYZRPY(:,3,traj), 'o-', 'LineWidth',1);
end
hold off

grid on
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('Time (normalized)','Interpreter','latex','FontSize',12)
ylabel('Y Coordinate','Interpreter','latex','FontSize',12)




