clear; clc; %close all;

%% Load data
nfiles = 6;
dmptype = 3;
for file=1:nfiles
    DataOS = table2array(readtable(['Real Data/traj_carte_',num2str(dmptype),num2str(file-1),'.csv']));
    RawData{file} = DataOS(2:end,:);
end


%% Remove identical points

threshold = 5e-3;
wrapRoll = 1;
for file=1:nfiles
    % Avoid jumps from -pi to pi in Roll
    if(wrapRoll)
    RawData{file}(:,5) = wrapTo2Pi(RawData{file}(:,5)+pi+0.2)-pi-0.2;
    RawData{file}(:,5) = max(RawData{file}(:,5), -pi);
    RawData{file}(:,5) = min(RawData{file}(:,5), +pi);
    end
    
    keep = sum(abs(diff(RawData{file})) >= threshold, 2) > 1;
    ttXYZRPY{file} = RawData{file}(keep,:);
    XYZRPY{file} = ttXYZRPY{file}(:,2:7);
end


%% Normalize in time

for file=1:nfiles
    ti = ttXYZRPY{file}(:,1);
    tXYZRPY{file}(:,1) = (ti-min(ti))/(max(ti)-min(ti));
    tXYZRPY{file}(:,2:7) = XYZRPY{file};
end


%% Interpolate to same instants

Npt = 301;
%Npt = 31;
tpt = linspace(0,1,Npt);
NXYZRPY = zeros(Npt, 7, nfiles);

for file=1:nfiles
    Trajf = tXYZRPY{file};
    NXYZRPY(:,1,file) = tpt;
    for i=1:Npt
        tprev = sum(Trajf(:,1)<=tpt(i));
        if tprev < length(Trajf(:,1))
            t_lerp = (tpt(i)-Trajf(tprev,1))/(Trajf(tprev+1,1)-Trajf(tprev,1));
            NXYZRPY(i,2:7,file) = t_lerp*(Trajf(tprev+1,2:7)-Trajf(tprev,2:7)) + Trajf(tprev,2:7);
        else
            t_lerp = 0;
            NXYZRPY(i,2:7,file) = Trajf(tprev,2:7);
        end
        
    end
end


%% Mean of all trajectories

MXYZRPY = mean(NXYZRPY,3);


%% Export mean trajectory

%writematrix(MXYZRPY, ['traj_carte_',num2str(dmptype),'m.csv']);


%% Plots

% Original data:
%{
fig1 = figure(1);
fig1.Color = [1,1,1];

plot3(XYZRPY{1}(:,1), XYZRPY{1}(:,2), XYZRPY{1}(:,3), 'LineWidth',1);
hold on
for traj=2:nfiles
    plot3(XYZRPY{traj}(:,1), XYZRPY{traj}(:,2), XYZRPY{traj}(:,3), 'LineWidth',1);
end
hold off
grid on
axis equal
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);
%}


% Normalized and filtered by coordinate:
%{
fig2 = figure(2);
fig2.Color = [1,1,1];

subplot(2,3,1)
hold on
for traj=1:nfiles
    plot(tXYZRPY{traj}(:,1), tXYZRPY{traj}(:,2))
end
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');

subplot(2,3,2)
hold on
for traj=1:nfiles
    plot(tXYZRPY{traj}(:,1), tXYZRPY{traj}(:,3))
end
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');

subplot(2,3,3)
hold on
for traj=1:nfiles
    plot(tXYZRPY{traj}(:,1), tXYZRPY{traj}(:,4))
end
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');

subplot(2,3,4)
hold on
for traj=1:nfiles
    plot(tXYZRPY{traj}(:,1), tXYZRPY{traj}(:,5))
end
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');

subplot(2,3,5)
hold on
for traj=1:nfiles
    plot(tXYZRPY{traj}(:,1), tXYZRPY{traj}(:,6))
end
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');

subplot(2,3,6)
hold on
for traj=1:nfiles
    plot(tXYZRPY{traj}(:,1), tXYZRPY{traj}(:,7))
end
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');
%}


% Interpolated data:
%{
fig3 = figure(3);
fig3.Color = [1,1,1];

plot(NXYZRPY(:,1,1), NXYZRPY(:,2:4,1))
hold on
for traj=2:nfiles
    plot(NXYZRPY(:,1,traj), NXYZRPY(:,2:4,traj))
end
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');

fig4 = figure(4);
fig4.Color = [1,1,1];

plot3(NXYZRPY(:,2,1), NXYZRPY(:,3,1), NXYZRPY(:,4,1), 'LineWidth',1);
hold on
for traj=2:nfiles
    plot3(NXYZRPY(:,2,traj), NXYZRPY(:,3,traj), NXYZRPY(:,4,traj), 'LineWidth',1);
end
hold off
grid on
axis equal
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);
%}


% Cluster and mean

fig5 = figure(5);
fig5.Color = [1,1,1];
fig5.Position = [100   700   700   600];

plot3(NXYZRPY(:,2,1), NXYZRPY(:,3,1), NXYZRPY(:,4,1),'--');
hold on
for traj=1:nfiles
    plot3(NXYZRPY(:,2,traj), NXYZRPY(:,3,traj), NXYZRPY(:,4,traj),'--');
end
lm = plot3(MXYZRPY(:,2), MXYZRPY(:,3), MXYZRPY(:,4),'m','LineWidth',2);
hold off
grid on
axis equal
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);

legend(lm, 'Final mean trajectory','Interpreter','latex',...
       'Location','SouthOutside','FontSize',12)
title('\textbf{Mean trajectory - Movement 2 (Low)}', ...
      'Interpreter','latex','FontSize',16)


fig6 = figure(6);
fig6.Color = [1,1,1];
fig6.Position = [100   100   1200   500];

subplot(2,3,1)
hold on
for traj=1:nfiles
    plot(NXYZRPY(:,1,traj), NXYZRPY(:,2,traj))
end
plot(MXYZRPY(:,1), MXYZRPY(:,2),'m','LineWidth',2);
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('Time (normalized)','Interpreter','latex','FontSize',12)
ylabel('X','Interpreter','latex','FontSize',12)

subplot(2,3,2)
hold on
for traj=1:nfiles
    plot(NXYZRPY(:,1,traj), NXYZRPY(:,3,traj))
end
plot(MXYZRPY(:,1), MXYZRPY(:,3),'m','LineWidth',2)
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('Time (normalized)','Interpreter','latex','FontSize',12)
ylabel('Y','Interpreter','latex','FontSize',12)
title('\textbf{Mean trajectory per coordinate (Movement 1)}', ...
      'Interpreter','latex','FontSize',16)

subplot(2,3,3)
hold on
for traj=1:nfiles
    plot(NXYZRPY(:,1,traj), NXYZRPY(:,4,traj))
end
plot(MXYZRPY(:,1), MXYZRPY(:,4),'m','LineWidth',2);
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('Time (normalized)','Interpreter','latex','FontSize',12)
ylabel('Z','Interpreter','latex','FontSize',12)

subplot(2,3,4)
hold on
for traj=1:nfiles
    plot(NXYZRPY(:,1,traj), NXYZRPY(:,5,traj))
end
plot(MXYZRPY(:,1), MXYZRPY(:,5),'m','LineWidth',2);
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('Time (normalized)','Interpreter','latex','FontSize',12)
ylabel('Roll','Interpreter','latex','FontSize',12)

subplot(2,3,5)
hold on
for traj=1:nfiles
    plot(NXYZRPY(:,1,traj), NXYZRPY(:,6,traj))
end
lm = plot(MXYZRPY(:,1), MXYZRPY(:,6),'m','LineWidth',2);
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('Time (normalized)','Interpreter','latex','FontSize',12)
ylabel('Pitch','Interpreter','latex','FontSize',12)

subplot(2,3,6)
hold on
for traj=1:nfiles
    plot(NXYZRPY(:,1,traj), NXYZRPY(:,7,traj))
end
plot(MXYZRPY(:,1), MXYZRPY(:,7),'m','LineWidth',2);
hold off
grid on
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('Time (normalized)','Interpreter','latex','FontSize',12)
ylabel('Yaw','Interpreter','latex','FontSize',12)
