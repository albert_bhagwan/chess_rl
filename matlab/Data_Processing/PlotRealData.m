clear; clc; close all;

fig1 = figure(1);
fig1.Color = [1,1,1];
fig1.Position = [525   317   932   808];


%% Load data (type 0) and plot
nfiles = 10;
dmptype = 0;
for file=1:nfiles
    DataOS = table2array(readtable(['Real Data/traj_carte_',num2str(dmptype),num2str(file-1),'.csv']));
    XYZRPY{:,:,file} = DataOS(2:end,2:7);
end

color = 'b';
l0 = plot3(XYZRPY{1}(:,1), XYZRPY{1}(:,2), XYZRPY{1}(:,3), color, 'LineWidth',1);
hold on
for traj=2:nfiles
    plot3(XYZRPY{traj}(:,1), XYZRPY{traj}(:,2), XYZRPY{traj}(:,3), color, 'LineWidth',1);
end


%% Load data (type 1) and plot
nfiles = 10;
dmptype = 1;
for file=1:nfiles
    DataOS = table2array(readtable(['Real Data/traj_carte_',num2str(dmptype),num2str(file-1),'.csv']));
    XYZRPY{:,:,file} = DataOS(2:end,2:7);
end

color = 'r';
l1 = plot3(XYZRPY{1}(:,1), XYZRPY{1}(:,2), XYZRPY{1}(:,3), color, 'LineWidth',1);
hold on
for traj=2:nfiles
    plot3(XYZRPY{traj}(:,1), XYZRPY{traj}(:,2), XYZRPY{traj}(:,3), color, 'LineWidth',1);
end


%% Load data (type 2) and plot
%{
dmptype = 2;
file = 1;
DataOS = table2array(readtable(['Real Data/traj_carte_',num2str(dmptype),num2str(file-1),'.csv']));
XYZRPY{:,:,file} = DataOS(2:end,2:7);
color = 'c';
l2 = plot3(XYZRPY{1}(:,1), XYZRPY{1}(:,2), XYZRPY{1}(:,3), color, 'LineWidth',1);
%}

%% Load data (type 3) and plot
nfiles = 6;
dmptype = 3;
for file=1:nfiles
    DataOS = table2array(readtable(['Real Data/traj_carte_',num2str(dmptype),num2str(file-1),'.csv']));
    XYZRPY{:,:,file} = DataOS(2:end,2:7);
end

color = 'm';
l3 = plot3(XYZRPY{1}(:,1), XYZRPY{1}(:,2), XYZRPY{1}(:,3), color, 'LineWidth',1);
hold on
for traj=2:nfiles
    plot3(XYZRPY{traj}(:,1), XYZRPY{traj}(:,2), XYZRPY{traj}(:,3), color, 'LineWidth',1);
end


%% Finish up
hold off

grid on
axis equal
set(gca, 'TickLabelInterpreter', 'latex');
xlabel('x','Interpreter','latex','FontSize',12);
ylabel('y','Interpreter','latex','FontSize',12);
zlabel('z','Interpreter','latex','FontSize',12);

legend([l0, l1, l3], 'Captured data for Movement 1', ...
    'Captured data for Movement 2 (High)', ...
    'Captured data for Movement 2 (Low)', ...
    'Interpreter','latex','FontSize',14, ...
    'Location','SouthOutside','Orientation','horizontal');



