#!/usr/bin/env python

from __future__ import print_function

from chess_rl.srv import DmpParser,DmpParserResponse
from chess_rl.msg import Pose
import rospy
import numpy as np

number = 0

def handle_dmp_parser(req):
    global number
    traj = np.genfromtxt(req.path+"/src/data/DMP_result_" + str(number) +".csv",delimiter=',').tolist()
    number += 1
    response = []
    for point in traj:
        response.append(Pose(point))
    return DmpParserResponse(response)

def dmp_parser_server():
    rospy.init_node('dmp_parser_server')
    s = rospy.Service('dmp_parser', DmpParser, handle_dmp_parser)
    rospy.spin()

if __name__ == "__main__":
    dmp_parser_server()
