from __future__ import division
import numpy as np
import scipy.io as sio
import os

def dmp(xyzrpy_home,xyzrpy_goal,dmp_type,path,number):
    # Load data
    if dmp_type == 1:
        W = sio.loadmat(path+'/src/data/W_0.mat')['W']
        C = sio.loadmat(path+'/src/data/C_0.mat')['C']
        H = sio.loadmat(path+'/src/data/H_0.mat')['H']
        AMP = sio.loadmat(path+'/src/data/AMP_0.mat')['AMP']
        tau = 3
    elif dmp_type == 2: # ABB: change to files 3
        W = sio.loadmat(path+'/src/data/W_1.mat')['W']
        C = sio.loadmat(path+'/src/data/C_1.mat')['C']
        H = sio.loadmat(path+'/src/data/H_1.mat')['H']
        AMP = sio.loadmat(path+'/src/data/AMP_1.mat')['AMP']
        tau = 3
    else:
        W = sio.loadmat(path+'/src/data/W_4.mat')['W']
        C = sio.loadmat(path+'/src/data/C_4.mat')['C']
        H = sio.loadmat(path+'/src/data/H_4.mat')['H']
        AMP = sio.loadmat(path+'/src/data/AMP_4.mat')['AMP']
        tau = 1

    # Parameters
    dt = 0.01
    alpha_z = 25
    beta_z = alpha_z/4
    alpha_g = alpha_z/2
    alpha_x = alpha_z/3
    alpha_v = alpha_z
    beta_v = beta_z

    c = np.transpose(C)
    h = np.transpose(H)

    Y = np.zeros((int(tau/dt)+1,3,6))
    for ID in range(6):

        y0 = xyzrpy_home[ID]
        G = xyzrpy_goal[ID]

        A = AMP[0,:,ID].item()
        dG = AMP[1,:,ID].item()

        if (A/(abs(dG)+1e-10) > 2.0):
            s = 1.0
        else:
            s = (G-y0)/dG

        y = y0
        g = y0
        z = 0.0

        x = 1.0
        v = 0.0

        w = np.transpose(W[:,ID])

        for i in range((int(tau/dt))+1):

            psi = np.exp(-0.5*(np.power((x-c),2)*h))
            f = np.sum(v*w*psi)/np.sum(psi+1e-10) * s

            vd = (alpha_v*(beta_v*(0-x)-v))/tau
            xd = v/tau

            zd = (alpha_z*(beta_z*(g-y)-z)+f)/tau
            yd = z/tau
            ydd = zd/tau

            gd = alpha_g*(G-g)

            v  = v + vd*dt
            x  = x + xd*dt

            z  = z + zd*dt
            y  = y + yd*dt

            g  = g + gd*dt

            Y[i,:,ID] = np.array([y,yd,ydd])

    XYZRPY = Y[:,0,:]

    np.savetxt(path+"/src/data/DMP_result_" + str(number) +".csv", XYZRPY, delimiter=",")

    return XYZRPY.tolist()
