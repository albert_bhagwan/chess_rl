#!/usr/bin/env python

from __future__ import print_function

from chess_rl.srv import DmpParser,DmpParserResponse
from chess_rl.msg import Pose
from dmp_eval import dmp
from pi2_learning import pi2
import rospy

number = 0

def handle_dmp_parser(req):
    global number
    if req.pi2:
        traj = pi2(req.init,req.goal,req.obs,req.height,req.dmp_type,req.path,number)
    else:
        traj = dmp(req.init,req.goal,req.dmp_type,req.path,number)
    number += 1
    response = []
    for point in traj:
        response.append(Pose(point))
    return DmpParserResponse(response)

def dmp_parser_server():
    rospy.init_node('dmp_parser_server')
    s = rospy.Service('dmp_parser', DmpParser, handle_dmp_parser)
    rospy.spin()

if __name__ == "__main__":
    dmp_parser_server()
