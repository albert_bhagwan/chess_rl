from __future__ import division
import numpy as np
from numpy import matlib
import scipy.io as sio
import os

def pi2(home,goal,obs_pt,height,dmp_type,path,number):

    if dmp_type == 2:
        W = sio.loadmat(path+'/src/data/W_3.mat')['W']
        C = sio.loadmat(path+'/src/data/C_3.mat')['C']
        H = sio.loadmat(path+'/src/data/H_3.mat')['H']
        AMP = sio.loadmat(path+'/src/data/AMP_3.mat')['AMP']
    else:
        return -1

    # DMP Parameters
    dt  = 0.01
    tau = 3
    Ndt = int(tau/dt)+1

    K = 10
    Sigma = 2
    n_psi = W.shape[0]
    n_dmp = W.shape[1]

    xpe = np.array(range(1,n_psi+1))
    ype = 1 - (xpe-(n_psi/2))**2 / (n_psi/2)**2
    ype = matlib.repmat(ype, 6, 1)
    ype = np.transpose(ype)

    NU = 100

    Wnew = W
    Ju = np.zeros((NU,1))

    Weps = np.zeros((n_psi, n_dmp, K))

    XYZRPY = np.zeros((Ndt, n_dmp, K))
    ACC    = np.zeros((Ndt, n_dmp, K))
    PSI    = np.zeros((n_psi, Ndt, n_dmp, K))
    B      = np.zeros((n_psi, Ndt, n_dmp, K))

    for upd in range(NU):
      for k in range(K):
        noise = np.random.normal(0, Sigma, size=W.shape)
        Weps[:,:,k] = Wnew + ype*noise

        [XYZRPY[:,:,k], ACC[:,:,k], PSI[:,:,:,k], B[:,:,:,k]] = dmp_PI2(home,goal,dt,tau,Weps[:,:,k],C,H,AMP)

      Wnew = PI2_Update(XYZRPY, ACC, W, Weps, PSI, B, obs_pt, height)
      if (upd%10) == 1:
        print(upd)


    [XYZRPYu, ACCu, PSIu,Bu] = dmp_PI2(home,goal,dt,tau,Wnew,C,H,AMP)

    np.savetxt(path+"/src/data/DMP_result_" + str(number) +".csv", XYZRPYu, delimiter=",")

    return XYZRPYu.tolist()


def dmp_PI2(xyzrpy_home,xyzrpy_goal,dt,tau,W,C,H,AMP):


    # Parameters
    alpha_z = 25
    beta_z = alpha_z/4
    alpha_g = alpha_z/2
    alpha_x = alpha_z/3
    alpha_v = alpha_z
    beta_v = beta_z

    c = np.transpose(C)
    h = np.transpose(H)

    Y   = np.zeros((int(tau/dt)+1,3,6))
    PSI = np.zeros((1000, int(tau/dt)+1, 6))
    B   = np.zeros((1000, int(tau/dt)+1, 6))

    for ID in range(6):

        y0 = xyzrpy_home[ID]
        G = xyzrpy_goal[ID]

        A = AMP[0,:,ID].item()
        dG = AMP[1,:,ID].item()

        if (A/(abs(dG)+1e-10) > 2.0):
            s = 1.0
        else:
            s = (G-y0)/dG

        y = y0
        g = y0
        z = 0.0

        x = 1.0
        v = 0.0

        w = np.transpose(W[:,ID])

        for i in range((int(tau/dt))+1):

            psi = np.exp(-0.5*(np.power((x-c),2)*h))
            f = np.sum(v*w*psi)/np.sum(psi+1e-10) * s

            vd = (alpha_v*(beta_v*(0-x)-v))/tau
            xd = v/tau

            zd = (alpha_z*(beta_z*(g-y)-z)+f)/tau
            yd = z/tau
            ydd = zd/tau

            gd = alpha_g*(G-g)

            v  = v + vd*dt
            x  = x + xd*dt

            z  = z + zd*dt
            y  = y + yd*dt

            g  = g + gd*dt

            Y[i,:,ID] = np.array([y,yd,ydd])

            b = v*psi/np.sum(psi+1e-10) * s

            PSI[:,i,ID] = psi
            B[:,i,ID] = b

    XYZRPY = Y[:,0,:]
    ACC = Y[:,2,:]

    return [XYZRPY, ACC, PSI, B]

def costFcn(XYZRPY, ACC, W, obs_pt, height):

  Ndt   = XYZRPY.shape[0]
  n_dmp = XYZRPY.shape[1]
  K     = XYZRPY.shape[2]

  WQ = 1e8
  WR = 1

  J = np.zeros((Ndt, K))

  WF = 1e10
  FTh_O = height - 0.02 + 0.01 # height of chess piece - grasp offset + security margin = 0.07

  for k in range(K):
    j = np.zeros((Ndt))

    for i in range(n_dmp):
      j += 0.5 * WQ * ACC[:,i,k]**2
      j += 0.5 * WR * np.sum((np.ones((Ndt,1))*np.transpose(W[:,i]))**2, axis=1)

    XYZk = XYZRPY[:,0:3,k]
    dPt  = XYZk - obs_pt
    dist = np.sum(dPt**2, axis=1)
    odw = 1-(dist-min(dist))/(max(dist)-min(dist))
    zdw = FTh_O+obs_pt[2] - XYZk[:,2]
    Ft = odw * zdw**2

    javoid = WF * abs(Ft)

    J[:,k] = j + javoid

  return J

def PI2_Update(XYZRPY, ACC, W, Weps, PSI, B, obs_pt, height):

  Ndt   = XYZRPY.shape[0]
  n_dmp = XYZRPY.shape[1]
  K     = XYZRPY.shape[2]
  n_psi = W.shape[0]

  J = costFcn(XYZRPY, ACC, W, obs_pt, height)

  S = np.flip(np.cumsum(np.flip(J,0),axis=0),0)

  maxS = np.max(S, axis=1)
  minS = np.min(S, axis=1)

  h = 10
  expNum = np.transpose(matlib.repmat(minS,K,1))
  expDen = np.transpose(matlib.repmat((maxS-minS),K,1))
  expS = np.exp(-h*(S - expNum)/expDen)

  P = expS / np.transpose(matlib.repmat(np.sum(expS,1),K,1))

  PMeps = np.zeros((n_dmp, K, Ndt, n_psi))

  for j in range(n_dmp):
    for k in range(K):

      # Compute g'*eps
      basis = np.transpose(B[:,:,j,k])
      dth_eps = Weps[:,j,k] - W[:,j]
      gTeps = np.sum(basis * matlib.repmat(dth_eps,Ndt,1), axis=1)

      # Compute g'g
      gTg = np.sum(basis*basis,axis=1)

      # Compute P*M*eps = P*g*g'*eps/(g'g)
      PMeps_aux = (P[:,k]*gTeps/(gTg + 1e-10))
      PMeps[j,k,:,:] = basis * np.transpose(matlib.repmat(PMeps_aux, n_psi,1))


  # Parameter update per timestep
  dthetat = np.sum(PMeps, axis=1)

  # Time weighting matrix
  NdtW = np.array(range(Ndt,0,-1))
  TW = np.transpose(matlib.repmat(NdtW, n_psi,1)) * np.transpose(PSI[:,:,0,0])
  TW = TW/matlib.repmat(np.sum(TW,axis=0),Ndt,1)

  # Final parameter update per DMP
  dtheta_aux = np.tile(np.reshape(TW, (1,Ndt,n_psi)), (n_dmp,1,1))
  dtheta = np.sum(dthetat * dtheta_aux, axis=1)

  # Update parameters
  Wnew = W + np.transpose(dtheta)

  return Wnew
