#include <ros/ros.h>
#include <string>
#include <dynamic_reconfigure/server.h>
#include "geometry_msgs/Quaternion.h"
#include <chess_implementation/Rectify.h>
#include <chess_rl/RobotConfig.h>
#include <chess_implementation/ChessPieceInfo.h>
#include <chess_implementation/CheckFreeCells.h>
#include <chess_implementation/CheckContentCell.h>
#include <chess_rl/PlanChessAction.h>
#include <chess_rl/SetTrajectory.h>
#include <chess_rl/SetTolerance.h>
#include <chess_rl/CheckMovAction.h>
#include <chess_rl/SetRobot.h>
#include <chess_rl/ChessInputConfig.h>
#include <chesslab_setup/ffplan.h>
#include <std_srvs/Empty.h>

using namespace std;

string goal_cell; // Desired cell of chess piece
bool valid_cell = false; // Is the input cell valid?
int chess_piece; // ID of the chess piece to move
int obstacle; // ID of the obstacle to avoid

// Valid chessboard columns
vector<string> board_letters = {"A", "B", "C", "D", "E", "F", "G", "H"};

// Aruco ID corresponding to each piece by model name
map<string, int> piece_model_ID = { {"pawnB1",   201},
                                    {"pawnB2",   202},
                                    {"pawnB3",   203},
                                    {"pawnB4",   204},
                                    {"pawnB5",   205},
                                    {"pawnB6",   206},
                                    {"pawnB7",   207},
                                    {"pawnB8",   208},
                                    {"towerB1",  209}, //Rook
                                    {"towerB2",  210}, //Rook
                                    {"horseB1",  211}, //Knight
                                    {"horseB2",  212}, //Knight
                                    {"knightB1", 213}, //Bishop
                                    {"knightB2", 214}, //Bishop
                                    {"queenB",   215},
                                    {"kingB",    216},
                                    {"pawnW1",   301},
                                    {"pawnW2",   302},
                                    {"pawnW3",   303},
                                    {"pawnW4",   304},
                                    {"pawnW5",   305},
                                    {"pawnW6",   306},
                                    {"pawnW7",   307},
                                    {"pawnW8",   308},
                                    {"towerW1",  309}, //Rook
                                    {"towerW2",  310}, //Rook
                                    {"horseW1",  311}, //Knight
                                    {"horseW2",  312}, //Knight
                                    {"knightW1", 313}, //Bishop
                                    {"knightW2", 314}, //Bishop
                                    {"queenW",   315},
                                    {"kingW",    316} };

// Function that returns if the input is a valid chessboard cell
bool isValidCell(string cellCandidate){

  // Check if there are only two characters
  if (cellCandidate.size() != 2) {
    return false;
  }
  else {
    int row;

    try{
      row = stoi(cellCandidate.substr(1));
    }
    catch (exception& e){
      ROS_ERROR_STREAM("Second character of the cell should be and int number!");
      return false;
    }

    if (row<1 || row > 8) {
      return false;
    }

    string col = cellCandidate.substr(0,1);
    vector<string>::iterator it = find(board_letters.begin(), board_letters.end(), col);
    if (it == board_letters.end()){
      return false;
    }

    return true;
  }
}

// Function that returns the height according to a aruco ID
double get_height(int aruco){
  double height;

  if (((201 <= aruco) && (aruco <= 208)) || ((301 <= aruco) && (aruco <= 308))) {
    height = 0.04;
  }
  else if (((209 <= aruco) && (aruco <= 214)) || ((309 <= aruco) && (aruco <= 314))) {
    height = 0.06;
  }
  else{
    height = 0.08;
  }
  return height;
}

// Function to call the Trajectory Module and execute the movement
int executeLinTraj(chess_rl::RobotConfig partial_goal,
                        string piece_to_move, bool return_home,
                        string robot_name,
                        ros::ServiceClient TrajClient,
                        ros::ServiceClient TolClient,
                        ros::ServiceClient CallClient,
                        std::vector<double> obstacle,
                        double height = 0.0){
  // With a partial goal, calls services to get trajectory and execute it
  // Needs one "set trajectory" service client:
  // - clt_joint_lin_traj (chess_implementation::SetTrajectory) -> TrajClient
  // - clt_carte_lin_traj (chess_implementation::SetTrajectory) -> TrajClient
  // And tolerance & call service clients:
  // - clt_set_tolerance (chess_implementation::SetTolerance) -> TolClient
  // - clt_call_action (std_srvs::Empty) -> CallClient


  // 1) Set trajectory using service
  chess_rl::SetTrajectory::Request traj_req;
  chess_rl::SetTrajectory::Response traj_resp;

  // Set final config of this step
  traj_req.final_config = partial_goal;
  traj_req.return_home = return_home;
  traj_req.arucoID = piece_model_ID[piece_to_move];
  traj_req.chess_piece = piece_to_move;
  traj_req.obs = obstacle;
  traj_req.height = height;

  if (!return_home) {
    ROS_INFO_STREAM("Goal [world frame] -> X: " << partial_goal.tcp.position.x << "; Y: " <<
                      partial_goal.tcp.position.y << "; Z: " << partial_goal.tcp.position.z);
  }
  else {
    ROS_INFO_STREAM("Returning to home configuration");
  }


  // Call the trajectory service
  int success_traj = TrajClient.call(traj_req, traj_resp);

  if(success_traj){
    if (traj_resp.collision_detected) {
      return 1;
    }
    else {
      ROS_INFO_STREAM("Trajectory set correctly");
    }
  }
  else{
      ROS_ERROR_STREAM("Failed to set trajectory");
      return -1;
  }

  // 2) Set tolerances using service
  chess_rl::SetTolerance::Request tol_req;
  chess_rl::SetTolerance::Response tol_resp;

  //Set goal tolerances
  tol_req.goal_tol.resize(7);
  tol_req.goal_tol[0].name = robot_name + "_elbow_joint";
  tol_req.goal_tol[1].name = robot_name + "_gripper_right_driver_joint";
  tol_req.goal_tol[2].name = robot_name + "_shoulder_lift_joint";
  tol_req.goal_tol[3].name = robot_name + "_shoulder_pan_joint";
  tol_req.goal_tol[4].name = robot_name + "_wrist_1_joint";
  tol_req.goal_tol[5].name = robot_name + "_wrist_2_joint";
  tol_req.goal_tol[6].name = robot_name + "_wrist_3_joint";

  tol_req.goal_tol[0].position = 0.001;
  tol_req.goal_tol[1].position = 0.001;
  tol_req.goal_tol[2].position = 0.001;
  tol_req.goal_tol[3].position = 0.001;
  tol_req.goal_tol[4].position = 0.001;
  tol_req.goal_tol[5].position = 0.001;
  tol_req.goal_tol[6].position = 0.001;

  tol_req.goal_tol[0].velocity = 0.1;
  tol_req.goal_tol[1].velocity = 0.1;
  tol_req.goal_tol[2].velocity = 0.1;
  tol_req.goal_tol[3].velocity = 0.1;
  tol_req.goal_tol[4].velocity = 0.1;
  tol_req.goal_tol[5].velocity = 0.1;
  tol_req.goal_tol[6].velocity = 0.1;

  // Set path tolerances
  tol_req.path_tol.resize(7);
  tol_req.path_tol[0].name = robot_name + "_elbow_joint";
  tol_req.path_tol[1].name = robot_name + "_gripper_right_driver_joint";
  tol_req.path_tol[2].name = robot_name + "_shoulder_lift_joint";
  tol_req.path_tol[3].name = robot_name + "_shoulder_pan_joint";
  tol_req.path_tol[4].name = robot_name + "_wrist_1_joint";
  tol_req.path_tol[5].name = robot_name + "_wrist_2_joint";
  tol_req.path_tol[6].name = robot_name + "_wrist_3_joint";

  tol_req.path_tol[0].position = 0.01;
  tol_req.path_tol[1].position = 0.01;
  tol_req.path_tol[2].position = 0.01;
  tol_req.path_tol[3].position = 0.01;
  tol_req.path_tol[4].position = 0.01;
  tol_req.path_tol[5].position = 0.01;
  tol_req.path_tol[6].position = 0.01;

  tol_req.path_tol[0].velocity = 100;
  tol_req.path_tol[1].velocity = 100;
  tol_req.path_tol[2].velocity = 100;
  tol_req.path_tol[3].velocity = 100;
  tol_req.path_tol[4].velocity = 100;
  tol_req.path_tol[5].velocity = 100;
  tol_req.path_tol[6].velocity = 100;

  // Set goal time tolerance
  tol_req.goal_time_tol = ros::Duration(10);

  // Call the tolerance service
  bool success_tol = TolClient.call(tol_req, tol_resp);

  if(success_tol){
      ROS_INFO_STREAM("Tolerances set correctly");
  }
  else{
      ROS_ERROR_STREAM("Failed to set tolerances");
      return -1;
  }

  // 3) Send command to execute trajectory
  std_srvs::Empty::Request call_req;
  std_srvs::Empty::Response call_resp;

  bool success_send = CallClient.call(call_req, call_resp);

  if(success_send){
      ROS_INFO_STREAM("Goal sent correctly. Executing.");
  }
  else{
      ROS_ERROR_STREAM("Failed to send goal");
      return -1;
  }

  return 0;
}

// Function to wait until the goal is reached
bool waitUntilGoalReached(ros::ServiceClient clt_check_mov_action){

  chess_rl::CheckMovAction::Request check_mov_req;
  chess_rl::CheckMovAction::Response check_mov_resp;

  bool finished = false;

  ROS_INFO_STREAM("Checking if follow joint action has finished...");
  while (!finished) {
    bool success_check_mov = clt_check_mov_action.call(check_mov_req, check_mov_resp);
    if (!success_check_mov) {
      ROS_ERROR_STREAM("Failed to check follow joint trajectory status");
      return false;
    }
    finished = check_mov_resp.finished;
  }
  if (check_mov_resp.error_code == 0) {
    ROS_INFO_STREAM("Finished with status: " << check_mov_resp.error_code);
  }
  else {
    ROS_ERROR_STREAM("Finished with status: " << check_mov_resp.error_code);
  }

  return true;
}

// Callbacks of the Dynamic Reconfigure input GUI
void DR_callback(chess_rl::ChessInputConfig &config, uint32_t level) {
  // Only check if checkbox marked
  if (config.send_command_B){
    if (isValidCell(config.goal_location_B)) {
      goal_cell = config.goal_location_B;
      valid_cell = true;

      config.goal_location_B = "";
    }
    else{
      ROS_ERROR_STREAM("Given goal cell is not valid, please try again!");
      valid_cell = false;
    }
    config.send_command_B = false;
  }
}

// -----------------------------------------------

// MAIN FUNCTION STARTS HERE
int main(int argc, char **argv) {

  // Initialize the ROS system and become a node.
  ros::init(argc, argv, "chess_manager");
  ros::NodeHandle n;

  // Define client objects to all services
  ros::ServiceClient clt_chess_piece_info = n.serviceClient<chess_implementation::ChessPieceInfo>("chess_piece_info");
  ros::ServiceClient clt_check_free_cells = n.serviceClient<chess_implementation::CheckFreeCells>("check_free_cells");
  ros::ServiceClient clt_check_content_cell = n.serviceClient<chess_implementation::CheckContentCell>("check_content_cell");
  ros::ServiceClient clt_planmovement = n.serviceClient<chesslab_setup::ffplan>("/chesslab_setup/planmovement");
  ros::ServiceClient clt_plan_chess_action = n.serviceClient<chess_rl::PlanChessAction>("plan_chess_action");
  ros::ServiceClient clt_joint_lin_traj = n.serviceClient<chess_rl::SetTrajectory>("joint_lin_traj");
  ros::ServiceClient clt_carte_lin_traj = n.serviceClient<chess_rl::SetTrajectory>("carte_lin_traj");
  ros::ServiceClient clt_set_tolerance = n.serviceClient<chess_rl::SetTolerance>("set_tolerance");
  ros::ServiceClient clt_call_action = n.serviceClient<std_srvs::Empty>("call_action");
  ros::ServiceClient clt_check_mov_action = n.serviceClient<chess_rl::CheckMovAction>("check_mov_action");
  ros::ServiceClient clt_set_robot = n.serviceClient<chess_rl::SetRobot>("set_robot");

  // Define dynamic reconfigure
  dynamic_reconfigure::Server<chess_rl::ChessInputConfig> server;
  dynamic_reconfigure::Server<chess_rl::ChessInputConfig>::CallbackType f;

  f = boost::bind(&DR_callback, _1, _2);
  server.setCallback(f);

  // Make sure all services are available
  ros::service::waitForService("chess_piece_info");
  ros::service::waitForService("check_free_cells");
  ros::service::waitForService("check_content_cell");
  ros::service::waitForService("/chesslab_setup/planmovement");
  ros::service::waitForService("plan_chess_action");
  ros::service::waitForService("joint_lin_traj");
  ros::service::waitForService("carte_lin_traj");
  ros::service::waitForService("set_tolerance");
  ros::service::waitForService("call_action");
  ros::service::waitForService("check_mov_action");
  ros::service::waitForService("set_robot");

  // -------------------------------------------
  // ------------  CHESS GAME CODE  ------------
  // -------------------------------------------


  // Main variables
  string current_cell;
  int goal_content;
  string robot_name;
  std::vector<double> obs_pt;
  obs_pt.resize(3);
  double height;

  if(!n.getParam("/chess_manager/chess_piece", chess_piece)) {
    ROS_ERROR("Need to define chess_piece parameter");
  }

  if(!n.getParam("/chess_manager/obstacle", obstacle)) {
    ROS_ERROR("Need to define obstacle parameter");
  }

  robot_name = "team_B";

  while (true) {
    // If command is invalid, must ask again and repeat
    ROS_INFO("Waiting for new command...");
    while (!valid_cell) {
      ros::spinOnce();

      if (valid_cell){
        // Check if the command is a valid move depending on goal cell
        chess_implementation::CheckContentCell::Request goal_cont_req;
        chess_implementation::CheckContentCell::Response goal_cont_resp;

        goal_cont_req.cell = goal_cell;

        bool success_goal_cont = clt_check_content_cell.call(goal_cont_req, goal_cont_resp);

        if(success_goal_cont){
          ROS_INFO_STREAM("Got cell content");
          // Save goal content (might be own piece, enemy piece or free)
          goal_content = goal_cont_resp.chess_piece;
          // If there is an obstacle in the goal, then it is not valid
          if (goal_content != 0) {
            valid_cell = false;
          }
        }
        else{
            ROS_ERROR_STREAM("Failed to get cell content");
            return -1;
        }
      }
    }

    // Get chess piece info (current cell)
    chess_implementation::ChessPieceInfo::Request info_req;
    chess_implementation::ChessPieceInfo::Response info_resp;

    info_req.ID = chess_piece;

    bool success_info = clt_chess_piece_info.call(info_req, info_resp);

    if(success_info){
        ROS_INFO_STREAM("Got chess piece info");
        current_cell = info_resp.cell;
    }
    else{
        ROS_ERROR_STREAM("Failed to get chess piece info");
        return -1;
    }

    // Get obstacle info
    info_req.ID = obstacle;

    success_info = clt_chess_piece_info.call(info_req, info_resp);

    if(success_info){
        ROS_INFO_STREAM("Got chess piece info");
        obs_pt[0] = info_resp.x;
        obs_pt[1] = info_resp.y;
        obs_pt[2] = get_height(obstacle)+0.21; // Offset added
    }
    else{
        ROS_ERROR_STREAM("Failed to get chess piece info");
        return -1;
    }

    height = get_height(chess_piece);

    // Call ffplan service
    chesslab_setup::ffplan::Request plan_req;
    chesslab_setup::ffplan::Response plan_resp;

    plan_req.init.objarucoid.push_back(chess_piece);
    plan_req.init.occupiedcells.push_back(current_cell);

    plan_req.init.freecells.push_back(goal_cell);

    plan_req.goal.objarucoid.push_back(chess_piece);
    plan_req.goal.occupiedcells.push_back(goal_cell);

    plan_req.goal.freecells.push_back(current_cell);

    bool success_plan = clt_planmovement.call(plan_req, plan_resp);

    if(success_plan){
        ROS_INFO_STREAM("Got chess plan");
    }
    else{
        ROS_ERROR_STREAM("Failed to get chess plan");
        return -1;
    }

    // Set robot to move (team_A or team_B)
    chess_rl::SetRobot::Request set_rob_req;
    chess_rl::SetRobot::Response set_rob_resp;

    set_rob_req.robot_name = "team_B";

    bool success_set_rob = clt_set_robot.call(set_rob_req, set_rob_resp);

    if(success_set_rob){
        ROS_INFO_STREAM("Robot name has been set");
    }
    else{
        ROS_ERROR_STREAM("Failed to set robot name");
        return -1;
    }

    int return_traj_code;

    chess_rl::PlanChessAction::Request action_req;
    chess_rl::PlanChessAction::Response action_resp;

    // Loop through all actions (pickup/putdown/throwaway) in plan
    for(int i = 0; i < plan_resp.plan.size(); i++) {
      action_req.action = plan_resp.plan[i];

      // Get partial goal sequence for each plan action
      bool success_action = clt_plan_chess_action.call(action_req, action_resp);

      if(success_action){
          ROS_INFO_STREAM("Planned chess action");
      }
      else{
          ROS_ERROR_STREAM("Failed to plan chess action");
          return -1;
      }

      // Piece model needed for collision checking
      string piece_to_move = action_resp.chess_piece;

      // Loop for all partial goals of the same action
      for (int j = 0; j < action_resp.partial_goals.size(); j++) {

        // Get partial goal (tcp & gripper)
        chess_rl::RobotConfig partial_goal = action_resp.partial_goals[j];

        // First action of the plan: home to safety in joint space
        if (i==0 && j==0) {
          return_traj_code = executeLinTraj(partial_goal, piece_to_move,
                                          false, robot_name, clt_joint_lin_traj,
                                          clt_set_tolerance, clt_call_action,
                                          obs_pt);

          if (return_traj_code != 0) {
            ROS_ERROR_STREAM("Failed to execute movement");
            return -1;
          }

          ROS_INFO_STREAM("Initial movement started correctly");

          if(!waitUntilGoalReached(clt_check_mov_action)) {
            return -1;
          }
        }
        // All other actions have to be linear in the cartesian/operational space
        else{
          return_traj_code = executeLinTraj(partial_goal, piece_to_move,
                                          false, robot_name, clt_carte_lin_traj,
                                          clt_set_tolerance, clt_call_action,
                                          obs_pt, height);


          if (return_traj_code == -1) {
            ROS_ERROR_STREAM("Failed to execute movement");
            return -1;
          }
          else if (return_traj_code == 0) {
            ROS_INFO_STREAM("Movement started correctly");
            if(!waitUntilGoalReached(clt_check_mov_action)) {
              return -1;
            }
          }
          else { // return_traj_code == 1
            ROS_ERROR("Collision detected...");
            return -1;
          }
        }
      }
    }

    // Additional goal not in plan: return home
    // Set empty partial goal, returning home does not need it
    chess_rl::RobotConfig partial_goal;
    string piece_to_move;

    return_traj_code = executeLinTraj(partial_goal, piece_to_move,
                                    true, robot_name, clt_joint_lin_traj,
                                    clt_set_tolerance, clt_call_action,
                                    obs_pt);

    if (return_traj_code != 0) {
      ROS_ERROR_STREAM("Failed to execute movement");
      return -1;
    }

    ROS_INFO_STREAM("Last movement started correctly");

    if(!waitUntilGoalReached(clt_check_mov_action)) {
      return -1;
    }

    // CHECK IF ARUCO IN DESIRED FINAL CELL
    // First, get location of moved piece
    chess_implementation::ChessPieceInfo::Request info_fin_req;
    chess_implementation::ChessPieceInfo::Response info_fin_resp;

    info_fin_req.ID = chess_piece;

    bool success_info_fin = clt_chess_piece_info.call(info_fin_req, info_fin_resp);

    // If we get response, check if the piece is now on the desired goal
    if(success_info_fin){
        ROS_INFO_STREAM("Got chess piece final info");
        if (info_fin_resp.cell == goal_cell) {
          ROS_INFO_STREAM("Movement checked: Piece " << chess_piece
                                << " is now in Cell " << goal_cell);
        }
        else{
          ROS_WARN_STREAM("Movement had unexpected result. Piece " << chess_piece
                       << " should be in Cell " << goal_cell
                       << ", but has location: " << info_fin_resp.cell);
        }
    }
    else{
        ROS_ERROR_STREAM("Failed to check movement result");
        return -1;
    }
    valid_cell = false; // In order to wait for new command
  }
  return 0;

}
