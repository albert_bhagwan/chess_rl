#include <ros/ros.h>
#include <sensor_msgs/JointState.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2_ros/transform_listener.h>
#include <tf/LinearMath/Matrix3x3.h>
#include "geometry_msgs/Quaternion.h"
#include "tf/transform_datatypes.h"
#include <fstream>
#include <sstream>
#include <iomanip>

sensor_msgs::JointState current_state_B;
std::ofstream traj_joints_csv;
std::ofstream traj_carte_csv;
ros::Time initial_time;
std::string file_name_joints;
std::string file_name_carte;
tf2_ros::Buffer tfBuffer;
geometry_msgs::TransformStamped base_tool_tfStamped;
std::string robot_name = "team_B";


// A callback function when a new joint state is received for robot B
void jointStateReceivedTeamB(const sensor_msgs::JointState& msg) {
  // Some messages arrive from chesslab_setup_node when detecting collisions
  // We want to filter them so we check if the joint name format is the same as expected
  if (msg.name[0] == "team_B_elbow_joint") {
    current_state_B = msg;
  }

  traj_joints_csv.open(file_name_joints, std::ios::app);
  ros::Duration t = current_state_B.header.stamp - initial_time;
  traj_joints_csv << t.toSec() << "," << current_state_B.position[3] << "," << current_state_B.position[2]
                        << "," << current_state_B.position[0] << "," << current_state_B.position[4]
                        << "," << current_state_B.position[5] << "," << current_state_B.position[6]
                        << "," << current_state_B.position[1];
  traj_joints_csv << "\n";
  traj_joints_csv.close();

  // ROS_INFO_STREAM("Joint 0: " << current_state_B.position[3]);
  // ROS_INFO_STREAM("Joint 1: " << current_state_B.position[2]);
  // ROS_INFO_STREAM("Joint 2: " << current_state_B.position[0]);
  // ROS_INFO_STREAM("Joint 3: " << current_state_B.position[4]);
  // ROS_INFO_STREAM("Joint 4: " << current_state_B.position[5]);
  // ROS_INFO_STREAM("Joint 5: " << current_state_B.position[6]);
  // ROS_INFO_STREAM("Joint 6: " << current_state_B.position[1]);

  std::vector<double> current_pose(6);
  tf::Quaternion quat;
  try{
    base_tool_tfStamped = tfBuffer.lookupTransform(robot_name + "_base_link", robot_name + "_tool0",
                              ros::Time(0));

    current_pose[0] = base_tool_tfStamped.transform.translation.x;
    current_pose[1] = base_tool_tfStamped.transform.translation.y;
    current_pose[2] = base_tool_tfStamped.transform.translation.z;

    tf::quaternionMsgToTF(base_tool_tfStamped.transform.rotation, quat);
    tf::Matrix3x3(quat).getRPY(current_pose[3], current_pose[4], current_pose[5]);

    // Debug info:
    // ROS_INFO_STREAM("Current [base_link frame] -> X: " << current_pose[0] << "; Y: "
    //                         << current_pose[1] << "; Z: " << current_pose[2]);
    // ROS_INFO_STREAM("Current [base_link frame] -> Roll: " << current_pose[3] << "; Pitch: "
    //                         << current_pose[4] << "; Yaw: " << current_pose[5]);
  }
  catch (tf2::TransformException &ex) {
    ROS_WARN("%s",ex.what());
    ros::Duration(1.0).sleep();
  }
  traj_carte_csv.open(file_name_carte, std::ios::app);
  traj_carte_csv << t.toSec() << "," << current_pose[0] << "," << current_pose[1]
                        << "," << current_pose[2] << "," << current_pose[3]
                        << "," << current_pose[4] << "," << current_pose[5];
  traj_carte_csv << "\n";
  traj_carte_csv.close();

}

int main(int argc, char **argv){
  // Initialize the ROS system and become a node.
  ros::init(argc, argv, "save_traj");
  ros::NodeHandle n;

  // Subscribe to tfs
  tf2_ros::TransformListener tfListener(tfBuffer);

  // Subscribe to joint_states
  ros::Subscriber sub_joint_states_team_B = n.subscribe("team_B_arm/joint_states", 1000, &jointStateReceivedTeamB);

  int n_file;
  std::string path_to_file;

  if(!n.getParam("/save_traj/n_file", n_file)) {
    ROS_ERROR("Need to define n_file parameter");
  }
  if(!n.getParam("/save_traj/path_to_file", path_to_file)) {
    ROS_ERROR("Need to define path_to_file parameter");
  }

  std::stringstream ss;
  ss << std::setw(2) << std::setfill('0') << n_file;
  file_name_joints = path_to_file + "/traj_joint_" + ss.str() + ".csv";
  file_name_carte = path_to_file + "/traj_carte_" + ss.str() + ".csv";

  traj_joints_csv.open(file_name_joints);
  traj_joints_csv << "Time,J0,J1,J2,J3,J4,J5,J6\n";
  traj_joints_csv.close();

  traj_carte_csv.open(file_name_carte);
  traj_carte_csv << "Time,X,Y,Z,R,P,Y\n";
  traj_carte_csv.close();

  ros::Rate rate(10);

  initial_time = ros::Time::now();

  while(ros::ok()){
		ros::spinOnce();
		rate.sleep();
	}
}
