# Robot Learning - Final Project: Learning human-like trajectories using the UR3 robotic arm
---

## Execute Demo

The launch files are provided to execute the first demo, to execute different available demos, please modify the launch files appropriately. Demo 1 can be launched with:

`roslaunch chess_rl demo_rl.launch`


---


***Albert Bhagwan & Adrià Luque***

*ETSEIB, UPC. 2021*

![ETSEIB](https://frm.eic.cat/se/img/logo-ETSEIB-UPC.png)
